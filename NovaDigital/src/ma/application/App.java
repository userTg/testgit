package ma.application;

import java.util.Date;

import ma.projet.bean.Developpeur;
import ma.projet.bean.Manager;

public class App {

	public static void main(String[] args) {
		
		Developpeur dev1 = new Developpeur(1, "Hill", "Terrence ", "th@gmail.com", 9876, 1700, new Date("1990/01/01"), "HTML/CSS");
		Developpeur dev2 = new Developpeur(2, "Spencer", " Bud ", "bd@gmail.com", 1357, 1800, new Date("1990/01/01"), "Java EE");

		Manager mag1 = new Manager(3, "Callahan", "Harry ", "hc#gmail.com ", 9753, 2100, new Date("1990/01/01"),"Achat");
		Manager mag2 = new Manager(3, "Gant", "Mitchell ", "mg#gmail.com ", 9393, 2300, new Date("1990/01/01"),"Finances");

		dev1.calculerSalaire(dev1.getSalaire());
		
		System.out.println(dev1.getDateNaissance());
		dev2.calculerSalaire(dev2.getSalaire());
		mag1.calculerSalaire(mag1.getSalaire());
		mag2.calculerSalaire(mag2.getSalaire());

		System.out.println(dev1.toString());
		System.out.println(dev1.calculerSalaire(dev1.getSalaire()));
		System.out.println(dev2.toString());
		System.out.println(dev2.calculerSalaire(dev2.getSalaire()));

		System.out.println(mag1.toString());
		System.out.println(mag1.calculerSalaire(mag1.getSalaire()));
		System.out.println(mag2.toString());
		System.out.println(mag2.calculerSalaire(mag2.getSalaire()));
	}

}
