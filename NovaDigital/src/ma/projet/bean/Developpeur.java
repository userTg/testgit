package ma.projet.bean;

import java.util.Date;

import ma.projet.Personne;

public class Developpeur extends Personne {

	private String specialite;

	public Developpeur(int id, String nom, String prenom, String mail, int tph, int salaire, Date dateNaissance, String specialite) {
		super(id, nom, prenom, mail, tph, salaire, dateNaissance);
		this.specialite = specialite;
	}

	public Developpeur() {
		super();
	}

	public String getSpecialite() {
		return specialite;
	}

	public void setSpecialite(String specialite) {
		this.specialite = specialite;
	}

	@Override
	public String toString() {
		return "Developpeur [specialite=" + specialite + ", getDateNaissance()=" + getDateNaissance() + ", getId()="
				+ getId() + ", getMail()=" + getMail() + ", getTph()=" + getTph() + ", getSalaire()=" + getSalaire()
				+ ", getCount()=" + getCount() + ", getNom()=" + getNom() + ", getPrenom()=" + getPrenom() + "]";
	}

	@Override
	public double calculerSalaire(int salaire) {
		double newSalaire = 1.2 * salaire;
		return newSalaire;
	}

}
