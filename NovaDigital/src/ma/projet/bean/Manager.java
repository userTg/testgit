package ma.projet.bean;

import java.util.Date;

import ma.projet.Personne;

public class Manager extends Personne {

	private String service;

	public Manager(int id, String nom, String prenom, String mail, int tph, int salaire, Date dateNaissance, String service) {
		super(id, nom, prenom, mail, tph, salaire, dateNaissance);
		this.service = service;
	}

	public Manager() {
		super();
	}

	public String getService() {
		return service;
	}

	public void setService(String service) {
		this.service = service;
	}

	

	@Override
	public String toString() {
		return "Manager [service=" + service + ", getDateNaissance()=" + getDateNaissance() + ", getId()=" + getId()
				+ ", getMail()=" + getMail() + ", getTph()=" + getTph() + ", getSalaire()=" + getSalaire()
				+ ", getCount()=" + getCount() + ", getNom()=" + getNom() + ", getPrenom()=" + getPrenom() + "]";
	}

	@Override
	public double calculerSalaire(int salaire) {
		double newSalaire = 1.35 * salaire;
		return newSalaire;
	}

}
