package ma.projet;

import java.text.SimpleDateFormat;
import java.util.Date;

public abstract class Personne {

	private int id;
	private String nom, prenom, mail;
	private int tph, salaire;
	private static int count;
	private Date dateNaissance;

	public Personne() {
		super();
	}

	

	public Personne(int id, String nom, String prenom, String mail, int tph, int salaire, Date dateNaissance) {
		super();
		this.id = id;
		this.nom = nom;
		this.prenom = prenom;
		this.mail = mail;
		this.tph = tph;
		this.salaire = salaire;
		this.dateNaissance = dateNaissance;
	}



	public Date getDateNaissance() {
		return dateNaissance;
	}



	public void setDateNaissance(Date dateNaissance) {
		this.dateNaissance = dateNaissance;
	}



	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public int getTph() {
		return tph;
	}

	public void setTph(int tph) {
		this.tph = tph;
	}

	public int getSalaire() {
		return salaire;
	}

	public void setSalaire(int salaire) {
		this.salaire = salaire;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	

	public abstract double calculerSalaire(int salaire);



	@Override
	public String toString() {
		SimpleDateFormat dateFormat = new SimpleDateFormat("d MMMM yyyy");
		String date = dateFormat.format(dateNaissance);
		return "Personne [id=" + id + ", nom=" + nom + ", prenom=" + prenom + ", mail=" + mail + ", tph=" + tph
				+ ", salaire=" + salaire + ", dateNaissance=" + dateNaissance + date;
	}

}
